FROM nginx:1.17.6

RUN mkdir -p /usr/share/nginx/html/ 

COPY ./ /usr/share/nginx/html/

EXPOSE 80

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]
